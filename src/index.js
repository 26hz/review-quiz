// Import Application class that is the main part of our PIXI project
import {
	Application
} from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import {
	Renderer
} from '@pixi/core' // Renderer is the class that is going to register plugins

import {
	BatchRenderer
} from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import {
	AppLoaderPlugin
} from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import {
	Sprite
} from '@pixi/sprite'

import {
	Container
} from '@pixi/display'

import {
	Graphics
} from '@pixi/graphics';
Renderer.registerPlugin('graphics', Graphics);

import * as Keyboard from 'pixi.js-keyboard';
import sound from 'pixi-sound';
import * as text from '@pixi/text';

import firebase from 'firebase/app';
import 'firebase/firestore';
// Firebase init
firebase.initializeApp({
	apiKey: "AIzaSyAhskAt0nPkPlNQpdlJgtzzUrCS0v5W64w",
	authDomain: "buyble-mvp.firebaseapp.com",
	databaseURL: "https://buyble-mvp.firebaseio.com",
	projectId: "buyble-mvp",
	storageBucket: "buyble-mvp.appspot.com",
	messagingSenderId: "129776807064",
	appId: "1:129776807064:web:d748c8fd2cc55de0b774ed"
});

const db = firebase.firestore();
const record = db.collection("quiz-questions").doc("kzwG8t3FoPUofaSRGkgW");


// App with width and height of the page
const app = new Application({
	width: window.innerWidth,
	height: window.innerHeight,
	transparent: false,
	resizeTo: window
});
// Create Canvas tag in the body
document.body.appendChild(app.view);
//add background color on canvas
app.renderer.backgroundColor = 0x4eccdf;

//declare global var
var levelNumber = 1,
	question, answerPosition, click = 0,
	bullet, bullets = [],
	thanksNote,
	answers = [],
	messageLeft, messageRight, messageRight2, plane1, bulletSpeed = 5,
	planets = [],
	planet1, planet2, planet3, percentageBar, innerBar, outerBar, closeButton,
	percentages = [25, 50, 25],
	percentageBars = [],
	percentage, planetHit = [0, 0, 0],
	percentageColor = [],
	recreatePercentage = false,
	state, planetDestroyed = [false, false, false],
	locationClicked;

app.loader
	.add('plane_1', "./assets/plane_1.png")
	.add('plane_2', "./assets/plane_2.png")
	.add('planet_1', "./assets/planet_1.png")
	.add('planet_2', "./assets/planet_2.png")
	.add('planet_3', "./assets/planet_4.png")
	.add('closeButton', "./assets/closeButton.png")
	.add('logo', "./assets/Logo_Blk.png")
	.add('bulletImage', "./assets/bullet.png")
app.loader.load(() => {
	record.get().then(function (doc) {
		var data = doc.data()

		//Text styles default desktop
		var style = new text.TextStyle({
			fontFamily: "Roboto",
			fontSize: 30,
			fill: "#fff",
			stroke: '#000000',
			strokeThickness: 4,
		});

		//question text set anchor and location
		question = new text.Text(data['question'], style);
		question.anchor.set(0.5);
		question.x = app.renderer.screen.width / 2;
		question.y = 35;
		//set question style
		question.style = {
			fill: '#000',
			strokeThickness: 2,
			fontSize: 40
		};

		//level quiz information
		messageLeft = new text.Text("Level : " + levelNumber, style);
		messageLeft.anchor.set(1);
		messageLeft.x = messageLeft.width + 45;
		messageLeft.y = app.renderer.screen.height - 20;

		//logo company
		messageRight = new text.Text("Power By :", style);
		messageRight2 = new Sprite.from('logo')
		messageRight.style = {
			fontSize: 20,
			strokeThickness: 0
		};
		messageRight.anchor.set(0.5, 1);
		messageRight.x = app.renderer.screen.width - messageRight.width - messageRight2.width;
		messageRight.y = app.renderer.screen.height - 25;

		messageRight2.anchor.set(0.5, 1);
		messageRight2.x = app.renderer.screen.width - messageRight2.width + 20;
		messageRight2.y = app.renderer.screen.height - 20;
		messageRight2.scale.x = 0.5;
		messageRight2.scale.y = 0.5;

		//add all texts and logo into the canvas
		app.stage.addChild(question);
		app.stage.addChild(messageLeft);
		app.stage.addChild(messageRight);
		app.stage.addChild(messageRight2);

		//add closeButton
		closeButton = new Sprite.from('closeButton')
		closeButton.anchor.set(0.5);
		closeButton.scale.x = 0.5;
		closeButton.scale.y = 0.5;
		closeButton.x = app.renderer.screen.width - closeButton.width + 10;
		closeButton.y = closeButton.height - 10;
		//close button action
		closeButton.pointertap = closeCanvas;
		closeButton.interactive = true;
		app.stage.addChild(closeButton);

		//show plane
		plane1 = new Sprite.from('plane_1')

		//show planets
		planet1 = new Sprite.from('planet_1')
		planet1.x = app.renderer.screen.width / 2;
		planet1.y = 100;
		planet1.scale.x = 0.4;
		planet1.scale.y = 0.4;
		planet2 = new Sprite.from('planet_2')
		planet2.x = app.renderer.screen.width / 2;
		planet2.y = app.renderer.screen.height / 2 - 50;
		planet2.scale.x = 0.4;
		planet2.scale.y = 0.4;
		planet3 = new Sprite.from('planet_3')
		planet3.x = app.renderer.screen.width / 2;
		planet3.y = app.renderer.screen.height - 200;
		planet3.scale.x = 0.4;
		planet3.scale.y = 0.4;
		planets.push(planet1, planet2, planet3)

		//add planets to canvas
		app.stage.addChild(planet1, planet2, planet3);

		//answer style
		var style2 = new text.TextStyle({
			fontFamily: "Roboto",
			fontSize: 30,
			fill: "#00000",
		});

		//show answers
		let textAnswer = [data['answer_1'], data['answer_2'], data['answer_3']]
		answerPosition = [
			planet1.x + planet1.width + 50,
			planet1.y + 20,
			planet2.x + planet2.width + 50,
			planet2.y + 20,
			planet3.x + planet3.width + 50,
			planet3.y + 20
		];

		//add answers data to the canvas and array
		for (var i = 0; i < 3; i++) {
			var answer = new text.Text(textAnswer[i], style2);
			answer.x = answerPosition[i * 2];
			answer.y = answerPosition[i * 2 + 1];

			answers.push(answer);
			// add it to the stage
			app.stage.addChild(answer);
		}

		//initializing sounds into the respective names
		sound.add({
			bulletSound: './assets/bulletSound.wav',
			destroyedSound: './assets/destroyedSound.wav',
		});

		responsive()
		showPlane()
	}).catch(function (error) {
		console.log("Error getting document:", error);
	});


})

//remove all elements in canvas
function closeCanvas() {
	for (var i = app.stage.children.length - 1; i >= 0; i--) {
		app.stage.removeChild(app.stage.children[i]);
	};
}

//function to detect collision
function hitTestRectangle(r1, r2) {

	//Define the variables we'll need to calculate
	let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

	//hit will determine whether there's a collision
	hit = false;

	//Find the center points of each sprite
	r1.centerX = r1.x + r1.width / 2;
	r1.centerY = r1.y + r1.height / 2;
	r2.centerX = r2.x + r2.width / 2;
	r2.centerY = r2.y + r2.height / 2;

	//Find the half-widths and half-heights of each sprite
	r1.halfWidth = r1.width / 2;
	r1.halfHeight = r1.height / 2;
	r2.halfWidth = r2.width / 2;
	r2.halfHeight = r2.height / 2;

	//Calculate the distance vector between the sprites
	vx = r1.centerX - r2.centerX;
	vy = r1.centerY - r2.centerY;

	//Figure out the combined half-widths and half-heights
	combinedHalfWidths = r1.halfWidth + r2.halfWidth;
	combinedHalfHeights = r1.halfHeight + r2.halfHeight;

	//Check for a collision on the x axis
	if (Math.abs(vx) < combinedHalfWidths) {

		//A collision might be occurring. Check for a collision on the y axis
		if (Math.abs(vy) < combinedHalfHeights) {

			//There's definitely a collision happening
			hit = true;
		} else {

			//There's no collision on the y axis
			hit = false;
		}
	} else {

		//There's no collision on the x axis
		hit = false;
	}

	//`hit` will be either `true` or `false`
	return hit;
};

//when screen is clicked
app.renderer.plugins.interaction.on('pointerup', movePlane);

//function to move the plane by clicks/taps
function movePlane(event) {
	click++;
	if (click > 1 && (event.data.global.x == locationClicked || (event.data.global.x - locationClicked <= 5 && event.data.global.x - locationClicked > 0) || (locationClicked - event.data.global.x <= 5 && locationClicked - event.data.global.x > 0))) {
		bullet = new Sprite.from('bulletImage')
		if (app.renderer.screen.width < 768) {
			bullet.scale.x = 0.3;
			bullet.scale.y = 0.3;
			bullet.position.x = plane1.x + plane1.width - 30;
			bullet.position.y = plane1.y - 5;
		} else {
			bullet.scale.x = 0.5;
			bullet.scale.y = 0.5;
			bullet.position.x = plane1.x + plane1.width - 40;
			bullet.position.y = plane1.y - 15;
		}
		sound.play('bulletSound');;
		app.stage.addChild(bullet);
		bullets.push(bullet);
	} else {
		plane1.x = event.data.global.x;
		plane1.y = event.data.global.y;
	}

	locationClicked = event.data.global.x;

	if (click == 1) {
		setTimeout(function () {
			click = 0
		}, 1000);
	}
}

//call function to animate the bullets
animateBullet();

function animateBullet() {
	requestAnimationFrame(animateBullet);

	//animation for all bullets
	for (var b = bullets.length - 1; b >= 0; b--) {
		//to detect collision between the bullets and the planets
		for (var p = planets.length - 1; p >= 0; p--) {
			//when collision happen
			if (hitTestRectangle(bullets[b], planets[p]) && !planetDestroyed[p]) {
				//show precentage of answers
				showPercentage(p);
				//eliminate the bullet (that was in collision)
				app.stage.removeChild(bullets[b])
				//remove the bullet from the bullets array
				bullets.splice(b, 1);
				//assign the planet that was hit
				planetHit[p] += 1;
				//when the planet was hit 5 times
				if (planetHit[p] == 5) {
					//planet destroyed and sound played
					planetDestroyed[p] = true;
					app.stage.removeChild(planets[p]);
					sound.play('destroyedSound');
				}
			} else {
				//when none collision happens, 
				//check screen size if it changes then the bullets size also changes
				if (app.renderer.screen.width < 768) {
					bullets[b].scale.x = 0.3;
					bullets[b].scale.y = 0.3;
				} else {
					bullets[b].scale.x = 0.5;
					bullets[b].scale.y = 0.5;
				}
				//move the bullets position by the speed
				bullets[b].position.x += bulletSpeed;
			}
		}
	}
	// render the container
	app.renderer.render(app.stage);
	Keyboard.update();
}

//trigger event when space bar is pressed
Keyboard.events.on('pressed', function (key) {
	if (key == 'Space') {
		//show bullet
		bullet = new Sprite.from('bulletImage')
		//decide the size from the screen width
		//decide the location by the plane
		if (app.renderer.screen.width < 768) {
			bullet.scale.x = 0.3;
			bullet.scale.y = 0.3;
			bullet.position.x = plane1.x + plane1.width - 30;
			bullet.position.y = plane1.y - 5;
		} else {
			bullet.scale.x = 0.5;
			bullet.scale.y = 0.5;
			bullet.position.x = plane1.x + plane1.width - 40;
			bullet.position.y = plane1.y - 15;
		}
		//play the sound
		sound.play('bulletSound');
		//add bullet to the canvas
		app.stage.addChild(bullet);
		//add bullet to the bullets array
		bullets.push(bullet);
	}
});

//show percentage of answers function
var showPercentage = function (planet) {
	//style from the text
	var style2 = new text.TextStyle({
		fontFamily: "Roboto",
		fontSize: 30,
		fill: "#00000",
	});

	//when the percentage is not shown yet
	if (percentageBars.length == 0) {
		//display thanks text
		thanksNote = new text.Text('Thank you for your feedback!', style2);
		//style decided from the screen size
		if (app.renderer.screen.width < 768) {
			thanksNote.style = {
				fontSize: 16
			};
			thanksNote.x = app.renderer.screen.width / 2;
			thanksNote.y = question.y + 25;
		} else {
			thanksNote.x = app.renderer.screen.width / 2;
			thanksNote.y = question.y + 40;
		}
		thanksNote.anchor.set(0.5);
		app.stage.addChild(thanksNote);

		//Create the percentage bar
		for (var j = 0; j < 3; j++) {
			percentageBar = new Container();
			app.stage.addChild(percentageBar);

			//Create the grey transparent background rectangle
			innerBar = new Graphics();
			innerBar.beginFill(0x000000, 0.1);

			percentage = new text.Text(percentages[j] + '%', style2);

			//Create the colored rectangle (percentage from the inner bar)
			outerBar = new Graphics();
			//the answer chosen will be given different color than the rest
			if (j == planet) {
				outerBar.beginFill(0x7abd9e, 0.5);
				percentageColor.push(0x7abd9e)
			} else {
				outerBar.beginFill(0xa0a18f, 0.5);
				percentageColor.push(0xa0a18f)
			}

			//when the screen is mobile
			if (app.renderer.screen.width < 768) {
				//change font style size
				percentage.style = {
					fontSize: 15
				}

				//reposition the percentage var
				percentageBar.position.set(answerPosition[j * 2] - planet1.width/2, answerPosition[j * 2 + 1] + 20)

				//redraw the inner bar
				innerBar.drawRoundedRect(0, 0, planet1.width, answers[0].height + 14, 10);

				//reposition the percentage text
				percentage.x = innerBar.x + innerBar.width - percentage.width - 5;
				percentage.y = innerBar.y + 5;

				//redraw the outer bar
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[j] / 100, answers[0].height + 14, 10);
			} else {
				//when the screen is desktop
				//reposition the percentageBar
				percentageBar.position.set(answerPosition[j * 2] - 30, answerPosition[j * 2 + 1] - 8)
				//change the font size
				percentage.style = {
					fontSize: 35
				}

				//redraw the innerbar
				innerBar.drawRoundedRect(0, 0, (app.renderer.screen.width / 2) - planet1.width - 70, answers[0].height + 20, 10);

				//reposition the percentage text
				percentage.x = innerBar.x + innerBar.width - 80;
				percentage.y = innerBar.y + 8;

				//redraw outerbar
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[j] / 100, answers[0].height + 20, 10);
			}

			innerBar.endFill();
			outerBar.endFill();

			//add the innerbar, pencentage text, and outerbar to percentageBar
			percentageBar.addChild(innerBar);
			percentageBar.addChild(percentage);
			percentageBar.addChild(outerBar);
			//add percentagebar outer with outerbar
			percentageBar.outer = outerBar;
			//add percentagebar to the percentageBars array
			percentageBars.push(percentageBar);
		}
	}
};

function showPlane() {
	// set the anchor point so the texture is centerd on the sprite
	plane1.anchor.set(0.5);

	// finally lets set the plane1 to be at a random position..
	plane1.x = app.renderer.screen.width / 8;
	plane1.y = app.screen.height / 4;

	//assign the velocity to 0 as initial
	plane1.vx = 0;
	plane1.vy = 0;

	// create a random speed for the plane1 between 0 - 2
	plane1.speed = 5;
	//add plane to the canvas
	app.stage.addChild(plane1);

	let left = keyboardArrows("ArrowLeft"),
		up = keyboardArrows("ArrowUp"),
		right = keyboardArrows("ArrowRight"),
		down = keyboardArrows("ArrowDown");

	//Left arrow key `press` method
	left.press = () => {
		//Change the plane1's velocity when the key is pressed
		plane1.vx = -5;
		plane1.vy = 0;
	};

	//Left arrow key `release` method
	left.release = () => {
		//If the left arrow has been released, and the right arrow isn't down,
		//and the plane1 isn't moving vertically:
		//Stop the plane1
		if (!right.isDown && plane1.vy === 0) {
			plane1.vx = 0;
		}
	};

	//Up
	up.press = () => {
		plane1.vy = -5;
		plane1.vx = 0;
	};
	up.release = () => {
		if (!down.isDown && plane1.vx === 0) {
			plane1.vy = 0;
		}
	};

	//Right
	right.press = () => {
		plane1.vx = 5;
		plane1.vy = 0;
	};
	right.release = () => {
		if (!left.isDown && plane1.vy === 0) {
			plane1.vx = 0;
		}
	};

	//Down
	down.press = () => {
		plane1.vy = 5;
		plane1.vx = 0;
	};
	down.release = () => {
		if (!up.isDown && plane1.vx === 0) {
			plane1.vy = 0;
		}
	};

	//Set the game state
	state = play;

	//Start the game loop 
	app.ticker.add(delta => gameLoop(delta));
}

//keyboard when arrows are clicked (up, right, left, and down)
function keyboardArrows(value) {
	let key = {};
	key.value = value;
	key.isDown = false;
	key.isUp = true;
	key.press = undefined;
	key.release = undefined;
	//The `downHandler`
	key.downHandler = event => {
		if (event.key === key.value) {
			if (key.isUp && key.press) key.press();
			key.isDown = true;
			key.isUp = false;
			event.preventDefault();
		}
	};

	//The `upHandler`
	key.upHandler = event => {
		if (event.key === key.value) {
			if (key.isDown && key.release) key.release();
			key.isDown = false;
			key.isUp = true;
			event.preventDefault();
		}
	};

	//Attach event listeners
	const downListener = key.downHandler.bind(key);
	const upListener = key.upHandler.bind(key);

	window.addEventListener(
		"keydown", downListener, false
	);
	window.addEventListener(
		"keyup", upListener, false
	);

	// Detach event listeners
	key.unsubscribe = () => {
		window.removeEventListener("keydown", downListener);
		window.removeEventListener("keyup", upListener);
	};

	return key;
}

function gameLoop(delta) {
	//Update the current game state:
	state(delta);
}

function play(delta) {
	//Use the plane1's velocity to make it move
	plane1.x += plane1.vx;
	plane1.y += plane1.vy
}

function responsive() {
	var w = app.renderer.screen.width;

	//when screen is mobile
	if (w < 768) {
		//change question font size
		question.style = {
			fontSize: 20
		};
		//reposition question text
		question.x = app.renderer.screen.width / 2;
		question.y = 25;

		//change fontsize of level text
		messageLeft.style = {
			fontSize: 18
		};
		//reposition the level text
		messageLeft.x = messageLeft.width + 15;
		messageLeft.y = app.renderer.screen.height - 20;
		//change font size for the company text
		messageRight.style = {
			fontSize: 12
		};
		//resize the logo image
		messageRight2.scale.x = 0.3;
		messageRight2.scale.y = 0.3;
		//reposition the level text
		messageRight.x = app.renderer.screen.width - messageRight2.width - 45;
		messageRight.y = app.renderer.screen.height - 20;
		//reposition the logo
		messageRight2.x = app.renderer.screen.width - messageRight2.width + 25;
		messageRight2.y = app.renderer.screen.height - 18;

		//resize the plane
		plane1.scale.x = 0.5;
		plane1.scale.y = 0.5;
		//when the plane is out from screen bring it back inside
		if (plane1.x > w) {
			//horizontally
			plane1.x = w - plane1.width;
		}
		if (plane1.y > app.renderer.screen.height) {
			//vertically
			plane1.y = app.renderer.screen.height - plane1.height;
		}

		//resize the planets
		planet1.scale.x = 0.3;
		planet1.scale.y = 0.3;
		planet2.scale.x = 0.3;
		planet2.scale.y = 0.3;
		planet3.scale.x = 0.3;
		planet3.scale.y = 0.3;

		//reposition the planets
		planet1.x = app.renderer.screen.width - planet1.width - 20;
		planet1.y = 70;
		planet2.x = app.renderer.screen.width - planet2.width - 20;
		planet2.y = app.renderer.screen.height / 2 - 50;
		planet3.x = app.renderer.screen.width - planet3.width - 20;
		planet3.y = app.renderer.screen.height - 170;

		//resize the close button
		closeButton.scale.x = 0.3;
		closeButton.scale.y = 0.3;
		//reposition the close button
		closeButton.x = app.renderer.screen.width - closeButton.width + 10;
		closeButton.y = closeButton.height - 10;

		//coordinates position of the answers
		answerPosition = [
			planet1.x + planet1.width/2,
			planet1.y + planet1.height + 10,
			planet2.x + planet2.width/2,
			planet2.y + planet2.height + 10,
			planet3.x + planet3.width/2,
			planet3.y + planet3.height + 10,
		]

		//change style for the percentage bar texts
		var style2 = new text.TextStyle({
			fontFamily: "Roboto",
			fontSize: 15,
			fill: "#00000",
		});

		//when thanks text is already displayed
		if (thanksNote != null) {
			//change font size
			thanksNote.style = {
				fontSize: 16
			};
			//reposition the text
			thanksNote.x = app.renderer.screen.width / 2 - thanksNote.width;
			thanksNote.y = question.y + 30;
		}

		//when percentage bars are already shown
		if (percentageBars.length > 0) {
			//erase all percentage bars
			percentageBars[0].removeChild(percentageBars[0].children[0], percentageBars[0].children[1], percentageBars[0].children[2]);
			percentageBars[1].removeChild(percentageBars[1].children[0], percentageBars[1].children[1], percentageBars[1].children[2]);
			percentageBars[2].removeChild(percentageBars[2].children[0], percentageBars[2].children[1], percentageBars[2].children[2]);
			percentageBars = [];

			//set the var to true
			recreatePercentage = true;
		}

		for (var i = 0; i < 3; i++) {
			//change font size
			answers[i].style = {
				fontSize: 13
			}
			//reposition the answers
			answers[i].x = answerPosition[i * 2] - answers[i].width/2;
			// answerPosition.splice( i*2, 0, answers[i].x);
			answers[i].y = answerPosition[i * 2 + 1 ];
			if (recreatePercentage) {
				//create percentage bar
				percentageBar = new Container();				

				//create inner bar
				innerBar = new Graphics();
				innerBar.beginFill(0x000000, 0.1);
				innerBar.drawRoundedRect(0, 0, planet1.width, answers[i].height + 10, 10);
				innerBar.endFill();

				//create the percentage texts
				percentage = new text.Text(percentages[i] + '%', style2);
				percentage.x = innerBar.x + innerBar.width - percentage.width - 5;
				percentage.y = innerBar.y + 5;

				//add two of them above to the percentage bar
				percentageBar.addChild(innerBar);
				percentageBar.addChild(percentage);

				//Create the colored rectangle
				outerBar = new Graphics();
				outerBar.beginFill(percentageColor[i], 0.5);
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[i] / 100, answers[i].height + 10, 10);
				outerBar.endFill();

				//add outer bar into the percentage bar
				percentageBar.addChild(outerBar);
				//add percentage bar outer with outer bar
				percentageBar.outer = outerBar;

				//reposition the percentage bar
				percentageBar.position.set(answerPosition[i * 2] - planet1.width, answerPosition[i * 2 + 1] + 20);

				//add percentage bar into the canvas
				app.stage.addChild(percentageBar);
				
				//add to the array
				percentageBars.push(percentageBar);
			}
		}
	} else if (w >= 768) {
		//change question font size
		question.style = {
			fontSize: 40
		};
		//reposition question text
		question.x = app.renderer.screen.width / 2;
		question.y = 35;
	
		//change font size of level text
		messageLeft.style = {
			fontSize: 30
		};
		//reposition the level text
		messageLeft.x = messageLeft.width + 45;
		messageLeft.y = app.renderer.screen.height - 20;
		//company text change font size
		messageRight.style = {
			fontSize: 20
		};
		//resize the logo image
		messageRight2.scale.x = 0.5;
		messageRight2.scale.y = 0.5;
		//reposition the company text
		messageRight.x = app.renderer.screen.width - messageRight.width - messageRight2.width;
		messageRight.y = app.renderer.screen.height - 25;
		//reposition the logo image
		messageRight2.x = app.renderer.screen.width - messageRight2.width + 20;
		messageRight2.y = app.renderer.screen.height - 20;

		//resize the plane
		plane1.scale.x = 1;
		plane1.scale.y = 1;

		//when the plane is out from the screen, bring it back
		if (plane1.x > w) {
			//horizontally
			plane1.x = w - plane1.width;
		}
		if (plane1.y > app.renderer.screen.height) {
			//vertically
			plane1.y = app.renderer.screen.height - plane1.height;
		}

		//reposition the planets
		planet1.x = app.renderer.screen.width / 2;
		planet1.y = 100;
		planet2.x = app.renderer.screen.width / 2;
		planet2.y = app.renderer.screen.height / 2 - 50;
		planet3.x = app.renderer.screen.width / 2;
		planet3.y = app.renderer.screen.height - 200;

		//resize the planets
		planet1.scale.x = 0.4;
		planet1.scale.y = 0.4;
		planet2.scale.x = 0.4;
		planet2.scale.y = 0.4;
		planet3.scale.x = 0.4;
		planet3.scale.y = 0.4;

		//resize the close button
		closeButton.scale.x = 0.5;
		closeButton.scale.y = 0.5;
		//reposition the close button
		closeButton.x = app.renderer.screen.width - closeButton.width + 10;
		closeButton.y = closeButton.height - 10;

		//coordinates position of the answers
		answerPosition = [
			planet1.x + planet1.width + 50,
			planet1.y + 20,
			planet2.x + planet2.width + 50,
			planet2.y + 20,
			planet3.x + planet3.width + 50,
			planet3.y + 20
		];

		//add font style for the percentage
		var style2 = new text.TextStyle({
			fontFamily: "Roboto",
			fontSize: 30,
			fill: "#00000",
		});

		//when the thank you text is already displayed
		if (thanksNote != null) {
			//change font size
			thanksNote.style = {
				fontSize: 30
			};
			//reposition thank you text
			thanksNote.x = app.renderer.screen.width / 2 - thanksNote.width;
			thanksNote.y = question.y + 20;
		}

		// when percentage bars are already shown
		if (percentageBars.length > 0) {
			//erase all percentage bars
			percentageBars[0].removeChild(percentageBars[0].children[0], percentageBars[0].children[1], percentageBars[0].children[2]);
			percentageBars[1].removeChild(percentageBars[1].children[0], percentageBars[1].children[1], percentageBars[1].children[2]);
			percentageBars[2].removeChild(percentageBars[2].children[0], percentageBars[2].children[1], percentageBars[2].children[2]);
			percentageBars = [];

			//set the var to true
			recreatePercentage = true;
		}

		for (var i = 0; i < 3; i++) {
			//reposition the answer
			answers[i].x = answerPosition[i * 2];
			answers[i].y = answerPosition[i * 2 + 1];
			answers[i].style = {
				fontSize: 30
			}

			if (recreatePercentage) {
				//create percentage bar
				percentageBar = new Container();
				//reposition the percentage bar
				percentageBar.position.set(answerPosition[i * 2] - 30, answerPosition[i * 2 + 1] - 8)
				//add percentage bar into the canvas
				app.stage.addChild(percentageBar);

				//create inner bar
				innerBar = new Graphics();
				innerBar.beginFill(0x000000, 0.1);
				innerBar.drawRoundedRect(0, 0, (app.renderer.screen.width / 2) - planet1.width * 2, 50, 10);
				innerBar.endFill();

				//create the percentage texts
				percentage = new text.Text(percentages[i] + '%', style2);
				percentage.x = innerBar.x + innerBar.width - 80;
				percentage.y = innerBar.y + 5;

				//add two of them above to the percentage bar
				percentageBar.addChild(innerBar);
				percentageBar.addChild(percentage);

				//Create the colored rectangle
				outerBar = new Graphics();
				outerBar.beginFill(percentageColor[i], 0.5);
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[i] / 100, 50, 10);
				outerBar.endFill();

				//add outer bar into the percentage bar
				percentageBar.addChild(outerBar);
				//add percentage bar outer with outer bar
				percentageBar.outer = outerBar;
				//add to the array
				percentageBars.push(percentageBar);
			}
		}
	}
}

function resize() {
	//resize canvas
	app.renderer.view.style.width = window.innerWidth + 'px';
	app.renderer.view.style.height = window.innerHeight + 'px';
	//call responsive function
	responsive()
}

//call function when screen is resized
window.onresize = resize;