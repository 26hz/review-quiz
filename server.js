require('isomorphic-fetch');
const dotenv = require('dotenv');
const Koa = require('koa');
const {
  default: createShopifyAuth,
  verifyRequest
} = require('@shopify/koa-shopify-auth');
const session = require('koa-session');

dotenv.config();

const port = parseInt(process.env.PORT, 10) || 3000;
// const dev = process.env.NODE_ENV !== 'production';

const {
  SHOPIFY_API_SECRET_KEY,
  SHOPIFY_API_KEY
} = process.env;

const server = new Koa();
server.use(session({
  secure: true,
  sameSite: 'none'
}, server));
server.keys = [SHOPIFY_API_SECRET_KEY];

server.use(
  createShopifyAuth({
    apiKey: SHOPIFY_API_KEY,
    secret: SHOPIFY_API_SECRET_KEY,
    scopes: ['read_products'],
    afterAuth(ctx) {
      const {
        shop,
        accessToken
      } = ctx.session;

      ctx.redirect('/');
    },
  }),
);

server.use(verifyRequest());
server.use(async (ctx) => {
  ctx.respond = false;
  ctx.res.statusCode = 200;
  return
});

server.listen(port, () => {
  console.log(`> Ready on http://localhost:${port}`);
});